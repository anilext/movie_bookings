require 'rails_helper'

RSpec.describe Booking, type: :model do
  #it {should validate_presence_of(:no_of_seats)}
  it { should belong_to(:user)}
  it { should belong_to(:screen)}
 # it { should belong_to(:balcony).optional }
  

  let!(:user1) {create(:user, name:"AAA", email:"aaa1@gmail.com", password:"123456", number:"5647382910")}
  let!(:user2) {create(:user, name: "BBB", email: "BBB@gmail.com", password: "123456", number: "7584937539")}

  let!(:theater) {create(:theater, name: "theater", location: "KPHB")}
    
  let!(:movie) {create(:movie, name:"RRR")}
  let!(:screen1) {create(:screen, capacity:30,  movie_id:movie.id, theater: theater, start_at: "2023-01-20 10:10:10")}
  let!(:screen2) {create(:screen, capacity:30,  movie_id:movie.id, theater: theater, start_at: "2023-01-20 10:10:10")}

  let!(:balcony1) {create(:balcony, price: 200, capacity: 20, screen:screen1)}

  let!(:booking) {create(:booking, no_of_seats:3, screen_id:screen1.id, user_id: user1.id, balcony:balcony1)}

  it "check model methods" do 
    
    avalible_seats = booking.check_seat_avalibility
    confirmed_seats = booking.confirmed_seats_no
   expect(avalible_seats).to eq(17)
   expect(confirmed_seats).to eq(3)
  end 

  it "check set_total " do 
   total = booking.set_total
   expect(total).to eq(600)
  end 


end
