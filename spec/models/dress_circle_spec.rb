require 'rails_helper'

RSpec.describe DressCircle, type: :model do
  it { should belong_to(:screen)}

  let!(:user1) {create(:user, name:"AAA", email:"aaa1@gmail.com", password:"123456", number:"5647382910")}
  let!(:user2) {create(:user, name: "BBB", email: "BBB@gmail.com", password: "123456", number: "7584937539")}

  let!(:theater) {create(:theater, name: "theater", location: "KPHB")}
    
  let!(:movie) {create(:movie, name:"RRR")}
  let!(:screen1) {create(:screen,  movie_id:movie.id, theater: theater, start_at: "2023-01-20 10:10:10")}
  let!(:screen2) {create(:screen, capacity:30,  movie_id:movie.id, theater: theater, start_at: "2023-01-20 10:10:10")}

  let!(:balcony1) {create(:balcony, price: 200, capacity: 20, screen:screen1)}
  let!(:dress_circle1) {create(:dress_circle, price:110, capacity: 700, screen:screen1)}

  it "" do 
    total_capacity = dress_circle1.set_seat_capacity_screen
    expect(total_capacity).to eq(true)
  end
end
