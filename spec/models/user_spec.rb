require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:number)}
  it { should validate_uniqueness_of(:number)}
  it { should validate_uniqueness_of(:email)}

  it { should have_many(:payments) }
  it { should have_many(:bookings)}

  
end
