require 'rails_helper'

RSpec.describe Theater, type: :model do 
	it { should validate_presence_of(:name)}

	it { should have_many(:screens)}
end 