require 'rails_helper'

RSpec.describe Screen, type: :model do 

	#it { should validate_presence_of(:capacity)}

	it { should belong_to(:theater)}
	it { should belong_to(:movie)}
end