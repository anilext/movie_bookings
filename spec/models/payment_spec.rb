require 'rails_helper'

RSpec.describe Payment, type: :model do
  it { should validate_presence_of(:total)} 

  it { should belong_to(:booking)}
  it { should belong_to(:user)}
end
