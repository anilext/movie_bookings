FactoryBot.define do
  factory :balcony do
    price { 1 }
    capacity { 1 }
  end
end
