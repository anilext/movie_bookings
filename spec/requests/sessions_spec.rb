require 'rails_helper'

RSpec.describe 'Session', type: :request do

  let!(:user1) {create(:user, name:"anil", email:"anil@gmail.com", password:"1234567", number:"9955110022")}
  let!(:user2) {create(:user, name:"BBB", email: "BBB@gmail.com", password: "12345678", number:"9332819473")}

  let!(:theater1) {create(:theater, name:"ABC", location:"KPHB")}
  let!(:theater2) {create(:theater, name:"CCC", location: "KPHB")}
  let!(:theater3) {create(:theater, name:"DDD", location: "Madhapur")}

  let!(:movie) {create(:movie, name:"RRR")}
  let!(:movie2) {create(:movie, name: "ABCD")}

  let!(:screen1) {create(:screen, capacity:450,  theater_id:theater1.id, movie_id: movie2.id)}
  let!(:screen2) {create(:screen, capacity:500,  theater_id:theater1.id, movie_id: movie.id)}
  let!(:screen3) {create(:screen, capacity:200,  theater_id: theater2.id, movie_id: movie2.id)}
  let!(:screen4) {create(:screen, capacity:200,  theater_id: theater3.id, movie_id: movie2.id)}


  describe "POST /create" do
  	it "user login" do 
      post "/login", params:{email:'anil@gmail.com', password:"1234567"}
      

      expect(json).not_to be_empty
      expect(response).to have_http_status(200)
      expect(json['user']['email']).to eq(user1.email)
      
      expect(json['token']).not_to be_empty
    end

    it " when email or password entered wrong" do 
    	post "/login", params:{email:"anil@gmail123.com", password:"1234567"}

    	expect(json['error']).to eq("your email/password is wrong")
    end 

  end

  describe "GET /search" do 
    it "when search by movie name " do 
      get "/search", params:{movie_name:"Abcd"}
      
      expect(json.count).to eq(3)
      expect(json[0]['id']).to eq(screen1.id)
      expect(json[1]['id']).to eq(screen3.id)
      expect(json[2]['id']).to eq(screen4.id)

    end 

    it "when search by location" do 
      get "/search?location=KPHB"
    
      expect(json.count).to eq(2)
      expect(json[0]['id']).to eq(theater1.id)
      expect(json[1]['id']).to eq(theater2.id)
    end
  end
end 