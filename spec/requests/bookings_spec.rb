require 'rails_helper'

RSpec.describe 'Booking', type: :request do 

	let!(:user1) {create(:user, name:"anil", email:"anil@gmail.com", password:"123456", number:"1234565432")}
	let!(:user2) { create(:user, name: "naruto", email: "naruto@email.com", password: "123456", number: "8765432987")}

	let!(:theater) {create(:theater, name:"theater 1", location: "madhapur")}

	let!(:movie) {create(:movie, name:"ABCD")}

	let!(:screen) {create(:screen, theater_id:theater.id, movie_id: movie.id,capacity: 650,  start_at:"2023-01-19 11:00:00")}
	let!(:screen1) {create(:screen, theater_id:theater.id, movie_id: movie.id, capacity: 10,  start_at: "2023-01-20 12:00:00")}

    let!(:balcony) {create(:balcony, screen_id:screen1.id, price: 160, capacity:5)}
    let!(:balcony1) {create(:balcony, screen_id:screen.id, price:200, capacity:200)}

    let!(:dress_circle1) {create(:dress_circle, price: 110, capacity:500, screen:screen)}
	let!(:booking) {create(:booking, no_of_seats:8, user_id:user2.id, screen_id:screen1.id,balcony_id:balcony.id)}
	let!(:booking1) {create(:booking, no_of_seats: 2, user_id:user1.id, screen_id: screen.id,balcony_id:balcony.id )}


	describe "GET /index" do
	it  "show user bookings" do 
		get "/bookings", headers:{Authorization: JsonWebToken.encode(user_id: user1.id)}
		
		expect(json).not_to be_empty
		expect(json['data'].count).to eq(1)
		expect(json['data'][0]['id']). to eq(booking1.id.to_s)
		expect(json['data'][0]['attributes']['no_of_seats']).to eq(booking1.no_of_seats)
		expect(json['data'][0]['attributes']['total']).to eq(booking1.total)

	end

	end 

	describe "POST /create" do 
		it "book tickets for movie balcony ticket" do 
			post "/bookings", params:{booking:{ no_of_seats:2, screen_id: screen.id, balcony_id:balcony.id}},
			                  headers: {Authorization: JsonWebToken.encode(user_id: user1.id)}
			total = Booking.last.set_total
			
			expect(json).not_to be_empty
			expect(json['data']['attributes']['no_of_seats']).to eq(2)
			expect(json['data']['attributes']['screen_id']).to eq(screen.id)
			expect(json['data']['attributes']['total']).to eq(total)
		end 
        it "book titckets in dress circle" do 
        	post "/bookings", params: {booking: {no_of_seats:3, screen_id:screen.id, dress_circle_id:dress_circle1.id}},
        	                  headers: {Authorization: JsonWebToken.encode(user_id: user2.id)}
        	
        	total = Booking.last.set_total
        	expect(json).not_to be_empty
        	expect(json['data']['attributes']['no_of_seats']).to eq(3)
        	expect(json['data']['attributes']['screen_id']).to eq(screen.id)
        	expect(json['data']['attributes']['total']).to eq(total)
        end 
        context "" do 

      	it "unavability of seats " do 
			post "/bookings", params:{ booking: {no_of_seats:5, screen_id: screen1.id, balcony_id:balcony1.id}},
			                  headers: {Authorization: JsonWebToken.encode(user_id: user2.id) }
		    

		    availble_seats = booking.check_seat_avalibility
		    expect(json['error']).to eq(" avalible seats are only #{availble_seats}")
		    
		end 
	end
	end 

	describe "GET /show" do 
		it "show particluar ticket bookinig only" do 
			get "/bookings/#{booking1.id}", headers: {Authorization: JsonWebToken.encode(user_id: user1.id)}
			
			total = booking1.set_total

			expect(json['data']['id']).to eq(booking1.id.to_s)
			expect(json['data']['attributes']['total']).to eq(total)
		end 
	end 

	describe "DELETE /destroy" do 
		it "user can delete thier ticket" do 
			delete "/bookings/#{booking.id}", headers: {Authorization: JsonWebToken.encode(user_id:user2.id)}
			
			expect(response).to have_http_status(204)
		end
	end
end 