# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Screen', type: :request do
  let!(:theater) { create(:theater, name: 'sri krishnadevarayaa', location: 'ongole') }
  let!(:movie) { create(:movie, name: 'RRR') }
  let!(:movie2) { create(:movie, name: 'pushpa') }

  let!(:screen) do
    create(:screen, capacity: 200, start_at: '2023-01-19 10:30:00',
                    theater_id: theater.id, movie_id: movie.id)
  end
  let!(:screen2) do
    create(:screen, capacity: 550, start_at: '2023-01-19 11:00:00',
                    theater_id: theater.id, movie_id: movie2.id)
  end
  let!(:balcony) { create(:balcony, price: 210, capacity: 150, screen: screen2) }
  let!(:dress_circle1) { create(:dress_circle, price: 110, capacity: 500, screen: screen2) }
  # let!(screen) {create(:screen)}

  describe 'GET /index' do
    it 'show all screens' do
      get '/screens', params: { theater_id: 1 }

      expect(json).not_to be_empty
      expect(json['data'].count).to eq(2)
      expect(json['data'][0]['id']).to eq(screen.id.to_s)
      expect(json['data'][1]['id']).to eq(screen2.id.to_s)
      expect(json['data'][0]['attributes']['capacity']).to eq(screen.capacity)
    end
  end

  describe 'GET /show' do
    it 'show particular screen' do
      get "/screens/#{screen2.id}"

      expect(json['data']['id']).to eq(screen2.id.to_s)
      expect(json['data']['relationships']['balcony']['data']['id']).to eq(balcony.id.to_s)
    end
  end
end
