require 'rails_helper'

RSpec.describe "Theater", type: :request do 

let!(:theater1) {create(:theater, name:"theater 1", location: "KPHB")}
let!(:theater2) {create(:theater, name: "Arjun", location: "KPHB")}
let!(:theater3) {create(:theater, name: "Siva paravathi", location:"KPHB")}
  describe "GET /index" do
    it " get all theaters" do 
    	get "/theaters"  
    	
    	expect(json['data'].count).to eq(3)
    	expect(json['data'][0]['attributes']['name']).to eq(theater1.name)
    	expect(json['data'][1]['attributes']['name']).to eq(theater2.name)
    	expect(json['data'][2]['attributes']['name']).to eq(theater3.name)

    end  
  end 

  describe "GET /show" do
    it "show particular theater" do 
    	get "/theaters/#{theater2.id}"
    	
    	expect(json).not_to be_empty
    	expect(json['data']['id']).to eq(theater2.id.to_s)
    	expect(json['data']['attributes']['name']).to eq(theater2.name)
    end 

    it "when record is not found" do     	
    	get "/theaters/99999" 
    	
    	expect(json['data']).to eq(nil)
    end 
  end	
end 