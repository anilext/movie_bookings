require 'rails_helper'

RSpec.describe 'Payment', type: :request do 

	let!(:user1) {create(:user, name:"AAA", email:"aaa1@gmail.com", password:"123456", number:"5647382910")}
	let!(:user2) {create(:user, name: "BBB", email: "BBB@gmail.com", password: "123456", number: "7584937539")}

	let!(:theater) {create(:theater, name: "theater", location: "KPHB")}
    
    let!(:movie) {create(:movie, name:"RRR")}
	let!(:screen1) {create(:screen, capacity:30,  movie_id:movie.id, theater: theater, start_at: "2023-01-20 10:10:10")}
    
    let!(:balcony1) {create(:balcony, capacity:15, screen:screen1, price:200)}
	let!(:booking1) {create(:booking, no_of_seats:2, screen_id: screen1.id, user_id:user2.id, balcony:balcony1)}
    let!(:booking) {create(:booking, no_of_seats:3, screen_id: screen1.id, user_id:user1.id)}

	let!(:payment) {create(:payment, user:user2, booking:booking, total:400)}
	describe "db" do 
		it "show all payments" do 
			get "/payments", headers: {Authorization: JsonWebToken.encode(user_id: user2.id)} 
			
			expect(json['data'].count).to eq(1)
			expect(json['data'][0]['id']).to eq(payment.id.to_s)
			expect(json['data'][0]['attributes']['total']).to eq(payment.total)
		end
	end

	describe "POSt /create" do 
		it "pay paments" do 
			post "/payments", params:{payment:{ total: 400, booking_id:booking1.id}}, 
			                  headers: {Authorization: JsonWebToken.encode(user_id: user2.id)}
			

            expect(json).not_to be_empty
            expect(json['data']['relationships']['booking']['data']['id']).to eq(booking1.id.to_s)
            expect(json['data']['attributes']['total']).to eq(400)
		end

		it "when try to pay with wrong total" do
		    post "/payments", params: {payment: {total: 200, booking_id: booking1.id}},
		                      headers: {Authorization: JsonWebToken.encode(user_id:user2.id)}
		                      
		    bill = booking1.total
		    expect(json['error']).to eq("your payment is declined. your bill is #{bill}")
		end

		it "when try to pay without login" do
		    post "/payments", params: {payment: {total: 400, booking_id:booking1.id}}

		     
		    expect(response).to have_http_status(403)
		end 
	end 
	
end 