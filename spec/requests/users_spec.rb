require 'rails_helper'

RSpec.describe "Users", type: :request do
  let!(:user1) {create(:user, name:"ldbcs", email:"khabsaj", number:"1234456734", password:"123123")}
  let!(:user2) {create(:user)}

  describe "GET /users" do
    it "to get all users" do
      
      get users_path
      
      expect(response).to have_http_status(200)
      expect(json).not_to be_empty
      expect(json['data'][0]['id']).to eq(user1.id.to_s)
      expect(json['data'][1]['id']).to eq(user2.id.to_s) 
      #expect(json)
    end
  end

  describe 'GET /show' do 
    it " show particular user" do 
      get "/users/#{user2.id}" 
      
      expect(json['data']['id']).to eq(user2.id.to_s)
      expect(json['data']['attributes']['email']).to eq(user2.email)
      expect(json['data']['attributes']['number']).to eq(user2.number)
    end
  end 

  describe 'POST /Create' do 
    it "create users" do 
      post "/users", params:{user: {name:"anil", email:"anil@gmail.com", number:"2345678912",
                                    password:"123456"}}
      
      expect(json['data']['attributes']['name']).to eq("anil")
      expect(json['data']['attributes']['email']).to eq("anil@gmail.com")
      expect(json['data']['attributes']['number']).to eq('2345678912')
    end

    it "without number to  create users" do 
      post '/users', params: {user: {name: "anil", email: "anil@gmail.com",
                                     password:"123456"}}
      
      expect(json['number']).to eq(["can't be blank"])
    end 
  end 

  describe "UPDATE /users" do 
    context " update user with validations " do 
      it " update user themselves only" do 
        put "/users/#{user1.id}", params:{user:{name: "update user"}},
                                  headers: {Authorization: JsonWebToken.encode(user_id:user1.id)}
        
        expect(json['data']['attributes']['name']).to eq("update user")
        expect(json['data']['attributes']['name']).not_to eq('ldbcs')
      end 

      it " forbid user to update another user" do 
        put "/users/#{user1.id}", params:{user:{name: "AAA"}},
                                  headers: {Authorization: JsonWebToken.encode(user_id: user2.id)}
        
        expect(response).to have_http_status(403)
      end
    end 

    it "false conditions to update user with already taken number" do 
      put "/users/#{user2.id}", params:{user:{number:"1234456734"}},
                                headers:{Authorization:JsonWebToken.encode(user_id:user2.id)}
      
      expect(json['number']).to eq(["has already been taken"])
    end 
  end 

end
