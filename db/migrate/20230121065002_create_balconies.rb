class CreateBalconies < ActiveRecord::Migration[5.2]
  def change
    drop_table :balconies
    create_table :balconies do |t|
      t.integer :price
      t.integer :capacity

      t.timestamps
    end
  end
end
