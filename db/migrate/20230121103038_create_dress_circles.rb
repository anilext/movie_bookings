class CreateDressCircles < ActiveRecord::Migration[5.2]
  def change
    create_table :dress_circles do |t|
      t.integer :price
      t.integer :capacity
      t.belongs_to :screen, foreign_key: true

      t.timestamps
    end
    add_column :bookings, :dress_circle_id, :integer
  end
end
