class CreateScreens < ActiveRecord::Migration[5.2]
  def change
    create_table :screens do |t|
      t.belongs_to :theater, foreign_key: true
      t.integer :capacity
      t.belongs_to :movie, foreign_key: true
      t.integer :price
      t.time :start_at

      t.timestamps
    end
  end
end
