class RemoveColumnSatrtAtScreens < ActiveRecord::Migration[5.2]
  def change
    remove_column :screens, :start_at, :time
    add_column :screens, :start_at, :datetime
  end
end
