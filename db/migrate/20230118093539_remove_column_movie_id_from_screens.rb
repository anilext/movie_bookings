class RemoveColumnMovieIdFromScreens < ActiveRecord::Migration[5.2]
  def change
    remove_column :screens, :movie_id, :integer
    add_column :screens, :movie_id, :integer, null:true
  end
end
