class AddColumnScreenToBalcony < ActiveRecord::Migration[5.2]
  def change
    add_column :balconies, :screen_id, :integer
  end
end
