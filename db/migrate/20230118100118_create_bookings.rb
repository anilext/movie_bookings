class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.integer :no_of_seats
      t.belongs_to :user, foreign_key: true
      t.belongs_to :screen, foreign_key: true

      t.timestamps
    end
  end
end
