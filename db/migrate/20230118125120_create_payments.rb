class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.integer :total
      t.belongs_to :user, foreign_key: true
      t.belongs_to :booking, foreign_key: true

      t.timestamps
    end
  end
end
