Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :users
  get 'theaters', to: 'theaters#index'
  get 'theaters/:id', to: 'theaters#show'
  get 'screens', to: 'screens#index'
  get 'screens/:id', to: 'screens#show'
  resources :bookings
  post 'payments', to: 'payments#create'
  get 'payments', to: 'payments#index'
  
  post 'login', to: 'sessions#create'

  get 'search', to: 'sessions#search'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
