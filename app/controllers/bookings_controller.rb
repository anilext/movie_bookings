# frozen_string_literal: true

class BookingsController < ApplicationController
  before_action :check_login
  before_action :set_booking, only: [:show, :destroy]
  def index
    bookings = current_user.bookings
    render json: BookingSerializer.new(bookings).serializable_hash

  end

  def show
       ticket = current_user.bookings.find(@booking.id)
       render json: BookingSerializer.new(ticket).serializable_hash
  end

  def create
    booking = current_user.bookings.new(booking_params)
    
    availble_seats = booking.check_seat_avalibility
    
    if availble_seats >= booking.no_of_seats
     if  booking.save
      render json: BookingSerializer.new(booking).serializable_hash
      end
    else
     render json: { error: " avalible seats are only #{availble_seats}" }
     
    end
  end

  def destroy
    ticket = current_user.bookings.find(@booking.id)
    ticket.destroy
  end

  private

  def booking_params
    params.require(:booking).permit( :screen_id, :no_of_seats, :balcony_id, :dress_circle_id)
  end

  def set_booking
      @booking = Booking.find(params[:id])
  end 
end
