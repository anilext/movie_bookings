class PaymentsController < ApplicationController
before_action :check_login
	def create 
		#bookings = current_user.bookings.find(params['payment'][:booking_id])
		
		payment = current_user.payments.create(payment_params)
		if payment.total == payment.booking.total 
			 payment.save
				render json: PaymentSerializer.new(payment).serializable_hash
			
		else 
			
			render json: {error: "your payment is declined. your bill is #{payment.booking.total}" }
		end
	end

	def index
	   payments = current_user.payments
	  #options = {inlcude: :booking}
	   render json: PaymentSerializer.new(payments).serializable_hash 
	end

	private 

	def payment_params
		
		params.require(:payment).permit( :booking_id, :total)
	end
end