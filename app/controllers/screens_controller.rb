# frozen_string_literal: true

class ScreensController < ApplicationController
  def index
    theater = Theater.find(params[:theater_id])
    ok= theater.screens 
    render json:ScreenSerializer.new(ok).serializable_hash
  end

  def show
    screens = []
     screen = Screen.find(params[:id])
    
    # screens << screen
    # screens<< {balcony:screen.balcony}

    # screens << screen.dress_circle
     render json:ScreenSerializer.new(screen).serializable_hash
    # render json:{ screen: screen, balcony_seats:screen.balcony, 
    #               dress_circle_seats:screen.dress_circle}
  end
end
 