# frozen_string_literal: true

class TheatersController < ApplicationController
  def index
    theaters = Theater.all
    render json: TheaterSerializer.new(theaters).serializable_hash
  end

  def show 
    theater = Theater.find_by(id:params[:id]) 
    render json: TheaterSerializer.new(theater).serializable_hash, status: :ok
  end
end
