# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :set_user, only: %i[show destroy update]
  before_action :check_owner, only: %i[update]
  def index
    @users = User.all
    render json: UserSerializer.new(@users).serializable_hash
  end

  def show

    render json: UserSerializer.new(@user).serializable_hash
  end

  def create
    user = User.create(user_params)
    if user.save
      render json: UserSerializer.new(user).serializable_hash
    else
      render json: user.errors
    end
  end

  def update
      if  @user.update(user_params)
         render json: UserSerializer.new(@user).serializable_hash
       else
         render json: @user.errors
         end      
  end

  def destroy; end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :email, :password, :number, images: [])
  end

  def check_owner
    head :forbidden unless @user.id == current_user.id
  end
end
