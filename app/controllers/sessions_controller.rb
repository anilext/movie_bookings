class SessionsController < ApplicationController
	def create 
		user = User.find_by_email(params[:email])

		if user && user.authenticate(params[:password])
			render json: {token: JsonWebToken.encode(user_id: user.id),
			              user: user}
		else
			render json: {error: "your email/password is wrong"}
		end
	end

	def search
	
       movie = Movie.where('name LIKE ?', "%#{params[:movie_name]}%").first
       theaters = Theater.where('location LIKE ?', "%#{params[:location]}%") if params[:location].present?
       screen = movie.screens

       if theaters.nil?

         render json:screen.to_json(include: :theater)
     else 
     	render json:theaters.to_json(include: :screens)
     end
       
	end 
end