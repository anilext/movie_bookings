# frozen_string_literal: true

class User < ApplicationRecord
  has_many :payments
  has_many :bookings
  validates :email, uniqueness: true
  validates :number, presence: true#, format: {with: /\d{10}/}
  validates :number, uniqueness: true

  has_many_attached :images
  has_secure_password
end
