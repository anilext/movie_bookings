# frozen_string_literal: true

class Booking < ApplicationRecord
  #validates :no_of_seats, presence: true
  belongs_to :user
  belongs_to :screen
  belongs_to :balcony, optional: true
  belongs_to :dress_circle, optional: true
  # validates :balcony_id, presence:false
  
  has_one :payments
  before_validation :set_total

  def set_total
    if balcony_id.present?
       self.total = no_of_seats * balcony.price
     elsif dress_circle_id.present?
        self.total = no_of_seats * dress_circle.price
     else
      return "select balcony or any other "
    end
  end

   def check_seat_avalibility
    screen_capacity - confirmed_seats_no
   end

  def screen_capacity
    Screen.find(screen_id).capacity
  end

  def confirmed_seats_no
    screens = Screen.find(screen_id)

    screens.bookings.sum(:no_of_seats)
  end
end
