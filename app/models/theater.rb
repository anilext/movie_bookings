class Theater < ApplicationRecord

	validates :name, presence: true
	has_many :screens

end
