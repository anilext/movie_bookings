class Screen < ApplicationRecord

  #validates :capacity, presence: true

  belongs_to :theater
  belongs_to :movie, optional: true

  has_one :balcony
  has_one :dress_circle

  has_many :bookings 

end
