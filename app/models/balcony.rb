class Balcony < ApplicationRecord

  belongs_to :screen
  before_validation :seats_capacity
  def seats_capacity 
    seats = 0
    seats = screen.dress_Circle.capacity if screen.dress_circle
    Screen.find(screen_id).update(capacity: capacity+seats) 
  end 

end
