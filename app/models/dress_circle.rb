class DressCircle < ApplicationRecord
  belongs_to :screen

  before_validation :set_seat_capacity_screen

  def set_seat_capacity_screen

    seats_for = 0
    seats_for = screen.balcony.capacity if screen.balcony
    Screen.find(screen_id).update(capacity: capacity + seats_for)
    #screen.update(capacity: capacity + seats_for) 
  end
end
