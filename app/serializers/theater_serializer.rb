class TheaterSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :location
  has_many :screens
end
