class BookingSerializer
  include FastJsonapi::ObjectSerializer
  attributes :no_of_seats, :total, :screen_id, :balcony_id, :dress_circle_id, :created_at
  # belongs_to :balcony 
  # belongs_to :dress_circle
end
