class ScreenSerializer
  include FastJsonapi::ObjectSerializer
  attributes :capacity, :start_at
  belongs_to :theater
  has_one :balcony 
  has_one :dress_circle
end
