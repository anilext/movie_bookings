class UserSerializer
  include Rails.application.routes.url_helpers

  include FastJsonapi::ObjectSerializer
  attributes  :name, :email, :number, :created_at

  attributes :image_url do |obj, params|
      #obj.images.map {|image| image.blob.service.path_for(image.key)}
        host = 'localhost:3000'
        url = obj.images.map{|image| host+Rails.application.routes.url_helpers.rails_blob_path(image, only_path: true) }
        #url.map{ |link| host+link }
  end

  end
