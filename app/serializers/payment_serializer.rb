class PaymentSerializer
  include FastJsonapi::ObjectSerializer
  attributes :total, :booking_id
  belongs_to :booking
end
